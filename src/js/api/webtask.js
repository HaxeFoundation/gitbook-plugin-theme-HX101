var app = new (require('express'))();
var wt = require('webtask-tools');
var AuthenticationClient = require('auth0@2.1.0').AuthenticationClient;

var auth0 = new AuthenticationClient({
    domain:      'haxe.eu.auth0.com',
    clientID:    'SLz3cxabk3hr6aOLNugd3kiHaNXSN5h1',
});

const RESPONSE = {
    OK : {
        statusCode : 200,
        message: "You have successfully subscribed to the newsletter!",
    },
    DUPLICATE : {
        status : 400,
        message : "You are already subscribed."
    },
    ERROR : {
        statusCode : 400,
        message: "Something went wrong. Please try again."
    },
    UNAUTHORIZED : {
        statusCode : 401,
        message : "You must be logged in to access this resource."
    }
};

app.get('/', function (req, res) {
    var accessToken = req.webtaskContext.accessToken;
    auth0.tokens.getInfo(accessToken).done(function(result){
        res.writeHead(200, { 'Content-Type': 'application/json'});
        res.end(JSON.stringify({
            "result": result,
            "token": accessToken,
            "message":"ok"
        }));
    });
});

app.get('/status', function (req, res) {
    res.end('GitLab ID is ' + req.user.username);
});

module.exports = wt.fromExpress(app).auth0({
    // send the user an appropriate message if the request is not authorized
    loginError: function (error, ctx, req, res, baseUrl) {
        res.writeHead(RESPONSE.UNAUTHORIZED.statusCode, { 'Content-Type': 'application/json'});
        res.end(JSON.stringify(RESPONSE.UNAUTHORIZED));
    }
});
