var Auth0 = require('auth0-js');
var $ = require('jquery');
var Cookies = require("cookies-js");

var origin = window.location.origin;
var cookieName = "auth0IdToken";
var auth0IdToken = null;
var userProfile = null;

var auth0 = new Auth0({
    domain:       'haxe.eu.auth0.com',
    clientID:     'SLz3cxabk3hr6aOLNugd3kiHaNXSN5h1',
    callbackURL:  origin,
    responseType: 'token'
});

function gotProfile(profile) {
    userProfile = profile;
    $("body").addClass("after-login");
    $(".username").html(profile.name);
    console.log(profile);

    $.ajax({
        type : 'GET',
        url : 'https://webtask.it.auth0.com/api/run/wt-andy-onthewings_net-0/hx101-api?webtask_no_cache=1',
        headers : {
            Authorization : 'Bearer ' + auth0IdToken
        }
    }).done(function(data) {
        console.log(data);
    });
}

function logout() {
    userProfile = null;
    Cookies.expire(cookieName);
    auth0.logout({returnTo: origin}, {version: 'v2'});
}

function init() {
    auth0IdToken = Cookies.get(cookieName);
    if (auth0IdToken) {
        auth0.getProfile(auth0IdToken, function (err, profile) {
            if (err) {
                console.error(err);
                auth0IdToken = null;
            } else {
                gotProfile(profile);
            }
        });
    }
    if (!auth0IdToken) {

    }
    $('a.login').click(function(e) {
        e.preventDefault();
        auth0.login({
            connection: "GitLab",
            popup: true
        },
        function(err, result) {
            if (err) {
                // don't alert error since there is also an error when the user closes the popup
                // alert("something went wrong: " + err);
                console.error(err);
                return;
            }
            auth0IdToken = result.idToken;
            Cookies.set(cookieName, auth0IdToken);
            auth0.getProfile(result.idToken, function (err, profile) {
                if (err) {
                    alert("something went wrong: " + err);
                    console.error(err);
                    return;
                } else {
                    gotProfile(profile);
                }
            });
        });
    });

    $(".username").click(function(e) {
        e.preventDefault();
        logout();
    });
}

module.exports = {
    init: init
};
