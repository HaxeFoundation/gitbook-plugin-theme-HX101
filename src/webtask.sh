#! /bin/bash

# Stop on first error
set -e

wt create --name hx101-api src/js/api/webtask.js \
    -s AUTH0_CLIENT_ID=$AUTH0_CLIENT_ID \
    -s AUTH0_CLIENT_SECRET=$AUTH0_CLIENT_SECRET \
    -s AUTH0_DOMAIN=$AUTH0_DOMAIN
